import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloWorldWindow {

	public JFrame frame;
	
	public HelloWorldWindow() {
	}

	public void initialize(boolean bool) {
		frame = new JFrame();
		frame.setBounds(200, 200, 200, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel lblHelloWorld = new JLabel(String.valueOf(bool));
		lblHelloWorld.setBounds(150, 150, 150, 150);
		frame.getContentPane().add(lblHelloWorld);
		
		frame.setVisible(true);
	}

}
